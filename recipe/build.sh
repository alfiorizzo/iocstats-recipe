#!/bin/bash

# LIBVERSION shall only include MAJOR.MINOR.PATCH for require
LIBVERSION=$(echo ${PKG_VERSION}| cut -d'.' -f1-3)

# Clean between variants builds
make clean

make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION}
# copy over ess specific substitutions
# not sure why this isn't done in the Makefile but in configure/module/RULES_MODULE...
install -m 644 ./template/*-ess.template       iocAdmin/Db/
install -m 644 ./template/*-ess.substitutions  iocAdmin/Db/
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} db
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} install
